﻿using System.Web.Mvc;
using System.Web.Routing;

namespace BrisbaneCityChiropractorMvc.Infrastructure
{
    public enum JavascriptPathType
    {
        Relative = 0,
        FullWebPath = 1
    };

    public enum MetaTagType
    {
        Description = 0,
        Keywords = 1
    };


    public static class BccChiroHtmlHelpers
    {
        public static MvcHtmlString Image(this HtmlHelper helper, string id, string url, string alternateText)
        {
            return Image(helper, id, url, alternateText, null);
        }

        public static MvcHtmlString Image(this HtmlHelper helper, string id, string url, string alternateText,
                                          object htmlAttributes)
        {
            // Instantiate a UrlHelper
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);

            // Create tag builder
            var builder = new TagBuilder("img");

            // Create valid id
            builder.GenerateId(id);

            // Add attributes
            builder.MergeAttribute("src", urlHelper.Content(url));
            builder.MergeAttribute("alt", alternateText);
            builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            // Render tag
            return new MvcHtmlString(builder.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString Script(this HtmlHelper helper, string jsFileName, JavascriptPathType pathType)
        {
            // Instantiate a UrlHelper
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);

            // Create tag builder
            var builder = new TagBuilder("script");
            
            // Add attributes
            switch (pathType)
            {
                case JavascriptPathType.Relative:
                    builder.MergeAttribute("src", urlHelper.Content("~/Scripts/" + jsFileName));
                    break;
                case JavascriptPathType.FullWebPath:
                    builder.MergeAttribute("src", urlHelper.Content(jsFileName));
                    break;
            }

            builder.MergeAttribute("type", "text/javascript");

            // Render tag. Note: script tags have to have an End tag and cannot be self-closing.
            return new MvcHtmlString(builder.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString Css(this HtmlHelper helper, string cssFilePath)
        {
            // Instantiate a UrlHelper
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);

            // Create tag builder
            var builder = new TagBuilder("link");

            // Add attributes
            builder.MergeAttribute("href", urlHelper.Content(cssFilePath));
            builder.MergeAttribute("rel", "stylesheet");
            builder.MergeAttribute("type", "text/css");

            // Render tag.
            return new MvcHtmlString(builder.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString MetaTag(this HtmlHelper helper, string content, MetaTagType metaTagType)
        {
            // Create tag builder
            var builder = new TagBuilder("meta");

            // Add attributes
            switch (metaTagType)
            {
                case MetaTagType.Description:
                    builder.MergeAttribute("name", "description");
                    break;
                case MetaTagType.Keywords:
                    builder.MergeAttribute("name", "keywords");
                    break;
            }

            builder.MergeAttribute("content", content);

            // Render tag.
            return new MvcHtmlString(builder.ToString(TagRenderMode.SelfClosing));
        }
    }
}