﻿using System.Web.Mvc;

namespace BrisbaneCityChiropractorMvc.Controllers
{
    public class HomeController : Controller
    {
        public ViewResult Index()
        {
            return View();
        }

        public ViewResult Clinic()
        {
            return View();
        }

        public ViewResult Practitioners()
        {
            return View();
        }

        public ViewResult ContactDetails()
        {
            return View();
        }

        public ViewResult PatientResources()
        {
            return View();
        }

        public RedirectResult Blog()
        {
            return new RedirectResult(@"http://brisbanecitychiro.blogspot.com.au/");
        }

        public ActionResult Articles()
        {
            return View();
        }

        public ActionResult LinksWeLike()
        {
            return View();
        }
    }
}
