﻿function initialize() {
    var infowindow = new google.maps.InfoWindow();
    var myLatlng = new google.maps.LatLng(-27.467533, 153.026559);
    var myMapCenter = new google.maps.LatLng(-27.467533, 153.026559);
    var myOptions = {
        zoom: 16,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    var map = new google.maps.Map(document.getElementById("map"), myOptions);
    
    map.setCenter(myMapCenter);

    //infowindow.setContent('<div style="margin:0px auto; width:64px;" ><img src="./Images/miniLogo.png" alt="JH Logo" /></div><strong><u>Jones Howard Accountants</u></strong><br />1/134 Constance Street<br />Fortitude Valley');
    
    infowindow.setPosition(myLatlng);
    infowindow.open(map);


    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: "Brisbane City Chiropracters"
    });

}